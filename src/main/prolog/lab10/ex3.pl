% same(+List1, +List2)
% are the two lists the same?
same([], []).
same([X|Xs], [X|Ys]) :- same(Xs, Ys).

% all_bigger(+List1, +List2)
% all elements in List1 are bigger than those in List2, 1 by 1
% example: all_bigger([10, 20, 30, 40], [9, 19, 29, 39]). -> yes
all_bigger([], []).
all_bigger([X|Xs], [Y|Ys]) :- X > Y,
                              all_bigger(Xs, Ys).

% sublist(+List1, +List2)
% List1 should be a subset of List2
% example: sublist([1, 2], [5, 3, 2, 1]). -> yes
sublist([], _).
sublist([X|Xs], Ys) :- member(X, Ys),
                       sublist(Xs, Ys).

% merge(+Lists, -List)
% merge([[1,2],[3,4],[5,6]], L).
% example: merge([[1, 2], [3, 4], [5, 6]], L). -> L/[1,2,3,4,5,6]
merge([], []).
merge([I|Is], OL) :- merge(Is, L),
                     append(I, L, OL).
