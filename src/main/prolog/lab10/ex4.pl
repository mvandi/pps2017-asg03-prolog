% seqR(+N, -List)
% example: seqR(4, [4, 3, 2, 1, 0]).
seqR(0, [0]).
seqR(N, [N|Xs]) :- N2 is N - 1, 
                   seqR(N2, Xs), 
                   !.

% seqR2(+N, -List)
% example: seqR2(4, [0, 1, 2, 3, 4]).
seqR2(N, N, [N]).
seqR2(N, M, [M|Xs]) :- M2 is M + 1, 
                       seqR2(N, M2, Xs), 
                       !.
seqR2(N, Xs)        :- seqR2(N, 0, Xs).

% inv(+List, -List)
% example: inv([1, 2, 3], [3, 2, 1]).
inv([], []).
inv([X|Xs], Ys) :- inv(Xs, Zs), 
                   append(Zs, [X], Ys).

% double(+List, -List)
% example: double([1, 2, 3], [1, 2, 3, 1, 2, 3]).
double(X, Y) :- append(X, X, Y).

% times(+List, +N, -List)
% example: times([1, 2, 3], 3, [1, 2, 3, 1, 2, 3, 1, 2, 3]).
times(_, 0, []).
times(X, N, Y) :- N2 is N - 1, 
                  times(X, N2, Z), 
                  !, 
                  append(Z, X, Y).

% proj(+List, -List)
% example: proj([[1, 2], [3, 4], [5, 6]], [1, 3, 5]).
proj([], []).
proj([[X|_]|Ys], [X|Zs]) :- proj(Ys, Zs).
