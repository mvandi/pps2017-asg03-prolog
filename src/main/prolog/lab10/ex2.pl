% sum(+List, -Sum)
sum(List, Sum)        :- sum(List, 0, Sum).
sum([], Acc, Sum)     :- Sum is Acc.
sum([X|Xs], Acc, Sum) :- A is Acc + X,
                         sum(Xs, A, Sum).

nmax(X, Y, X) :- X >= Y.
nmax(X, Y, Y) :- X < Y.

% max(+List, -Max)
% Max is the biggest element in List
% Suppose the list has at least one element
max([X|Xs], Max)       :- max(Xs, X, Max).
max([], Max, Max).
max([X|Xs], Temp, Max) :- nmax(X, Temp, NewMax),
                          max(Xs, NewMax, Max).

nmin(X, Y, X) :- X < Y.
nmin(X, Y, Y) :- X >= Y.

% min(+List, -Min)
% Min is the smallest element in List
% Suppose the list has at least one element
min([X|Xs], Min)       :- min(Xs, X, Min).
min([], Min, Min).
min([X|Xs], Temp, Min) :- nmin(X, Temp, NewMin),
                          min(Xs, NewMin, Min).
