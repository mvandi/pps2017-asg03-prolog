lookup([Head|_], zero, Head).
lookup([_|Tail], s(Pos), Elem) :- lookup(Tail, Pos, Elem).
