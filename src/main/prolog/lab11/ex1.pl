% Drops the first occurrence of element
% dropFirst(+Elem, +List, -OutList)
% example: dropFirst(10,[10,20,10,30,10],L). -> L/[20,10,30,10]
dropFirst(X, [X|T], T)      :- !.
dropFirst(X, [H|Xs], [H|L]) :- dropFirst(X, Xs, L).

% Drops the last occurrence of element
% dropLast(+Elem, +List, -OutList)
% example: dropLast(10,[10,20,10,30,10],L). -> L/[10,20,10,30]
dropLast(X, [X|Xs], [X|L]) :- member(X, Xs), 
                              !, 
                              dropLast(X, Xs, L).
dropLast(X, [X|T], T).
dropLast(X, [H|Xs], [H|L]) :- dropLast(X, Xs, L).

% Drops all the occurrences occurrences of element
% dropAll(+Elem, +List, -OutList)
% example: dropAll(10,[10,20,10,30,10],L). -> L/[20,30]
dropAll(X, [], []).
dropAll(X, [X|Xs], L)     :- !, 
                             dropAll(X, Xs, L).
dropAll(X, [H|Xs], [H|L]) :- dropAll(X, Xs, L).
