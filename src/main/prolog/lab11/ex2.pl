% Obtain a graph from a circular list.
%
% fromCircList(+List, -Graph)
% example: fromCircList([10,20,30],L). -> L/[e(10,20),e(20,30),e(30,10)]
fromCircList([T], H, [e(T, H)]).
fromCircList([H1, H2|T], H, [e(H1, H2)|L]) :- fromCircList([H2|T], H, L).
fromCircList([H|T], G)                     :- fromCircList([H|T], H, G).

% all the nodes that can be reached in 1 step from Node
% possibly use findall, looking for e(Node, _) combined
% with member(?Elem, ?List).
%
% reaching(+Graph, +Node, -List)
% example: reaching([e(1,2),e(1,3),e(2,3)],1,L). -> L/[2, 3], L[2, 2]
reaching([], e(N, _), []).
reaching([e(N, M)|T], e(N, _), [M|L]) :- reaching(T, e(N, _), L), !.
reaching([e(M, _)|T], e(N, _), L)     :- reaching(T, e(N, _), L).
reaching(G, N, L)                     :- reaching(G, e(N, _), L).

% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1.
%
% a path from N1 to N2 exists if there is a e(N1, N2).
%
% a path from N1 to N2 is OK if N3 can be reached from N1, 
% and then there is a path from N2 to N3, recursively.
%
% anypath(+Graph, +Node1, +Node2, -ListPath)
% example: anypath([e(1,2),e(1,3),e(2,3)],1,3,L). L -> L/[e(1,2),e(2,3)], L/[e(1,3)]
anypath([e(S, T)|_], S, T, [e(S, T)]).
anypath([e(S, N)|Xs], S, T, [e(S, N)|L]) :- anypath(Xs, N, T, L).
anypath([_|Xs], S, T, L)                 :- anypath(Xs, S, T, L).

toset(L, LL) :- setof(X, member(X, L), LL), !.

% all the nodes that can be reached from Node
% Suppose the graph is NOT circular!
% let R be the all the nodes that reach N, 
% find (recursively) all the nodes that reach
% the R nodes.
%
% allreaching(+Graph, +Node, -List)
% example: allreaching([e(1,2),e(2,3),e(3,5)],1,L). -> L/[2,3,5]
allreaching(G, N, L) :- findall(T, anypath(G, N, T, _), LL),
                        toset(LL, L).
