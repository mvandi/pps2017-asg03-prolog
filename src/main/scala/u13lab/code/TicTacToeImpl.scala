package u13lab.code

import java.io.FileInputStream

import alice.tuprolog.{Number, Term, Theory}
import u13lab.code.Scala2P._

/**
  * Created by mirko on 4/10/17.
  */
class TicTacToeImpl(fileName: String) extends TicTacToe {

  private val engine = mkPrologEngine(new Theory(new FileInputStream(fileName)))
  private var tboard: Term = _
  createBoard()

  override def createBoard() = {
    val goal = "retractall(board(_)),newboard(B),assert(board(B))"
    tboard = solveOneAndGetTerm(engine, goal, "B")
  }

  override def checkCompleted(): Boolean =
    solveWithSuccess(engine, "board(B),boardfilled(B)")


  override def checkVictory(): Boolean =
    solveWithSuccess(engine, "board(B),threeinarow(B)")


  override def isAFreeCell(i: Int, j: Int): Boolean =
    solveWithSuccess(engine, s"board(B),not(filledsquare(B,${i * 3 + j + 1}))")

  private def setCell(pos: Int, player: String): Unit = {
    val goal = s"retract(board(B)),!,setsquare(B,$pos,$player,B2),assert(board(B2))"
    tboard = solveOneAndGetTerm(engine, goal, "B2")
  }

  override def setHumanCell(i: Int, j: Int): Unit = {
    setCell(i * 3 + j + 1, "'X'")
  }

  override def setComputerCell(): Array[Int] = {
    val goal = s"response($tboard, 'O', Pos)"
    solveOneAndGetTerm(engine, goal, "Pos") match {
      case number: Number =>
        val pos = number.intValue
        setCell(pos, "'O'")
        Array((pos - 1) / 3, (pos - 1) % 3)
      case x: Any => throw new IllegalArgumentException(s"Illegal term: $x")
    }
  }

  override def toString: String =
    solveOneAndGetTerm(engine, "board(B)", "B").toString

}
