package u13lab.code

object Permutations extends App {

  // first an example of for-comprehension with streams..
  // note the first collection sets the type of all subsequent results

  val s: Stream[Int] = for (i <- (10 to 50 by 10).toStream; k = i + 1; j <- List(k - 1, k, k + 1)) yield j
  println(s) // Stream(10,?)
  println(s.take(10).toList) // a list with the first 10 results
  println(s) // the same stream, but now we know 10 elements of it
  println(s.toList) // all on list
  println(s) // the same stream, but now we know all its elements

  // now let's do permutations
  // fill this method remove such that it works as of the next println
  // - check e.g. how method "List.split" works
  def removeAtPos[A](list: List[A], n: Int) = list.filterNot(_ == list(n))

  println(removeAtPos(List(10, 20, 30, 40), 1)) // 10,30,40

  def permutations[A](list: List[A]): Stream[List[A]] = {
    if (list.size < 2) {
      Stream(list)
    } else {
      (for {
        x <- list
        y <- permutations(list filterNot (_ == x))
      } yield x :: y).toStream
    }
  }

  implicit class Factorial(private[this] val n: Int) {
    def ! : Int = {
      var result = 1
      for (i <- 2 to n)
        result *= i
      result
    }
  }

  /* here a for comprehension that:
     - makes i range across all indexes of list (converted as stream)
     - assigns e to element at position i
     - assigns r to the rest of the list as obtained from removeAtPos
     - makes pr range across all results of recursively calling permutations on r
     - combines by :: e with pr
   */

  val n = 4
  val list = List.iterate(10, n)(_ + 10)
  val perms = permutations(list).toList
  println(perms.size == (n !))
  println(perms)

}
